import Foundation

// *** Домашняя работа по теме 1. Переменные. Типы. Операторы

// Задача 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Задача 2
//let milkPrice: Int = 3

// Задача 3
let milkPrice: Double = 4.20

// Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0
profit = milkPrice * Double(milkBottleCount!)
print(profit)

// Задача 5
var employeesList = [String]()
employeesList = ["Иван", "Марфа", "Андрей", "Петр", "Генадий"]

// Задача 6
var isEveryoneWorkHard: Bool = false
var workingHours = 24

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print("isEveryoneWorkHard =", isEveryoneWorkHard)

// Дополнительное задание
/*
    Принудительное извлечение значения опцонала может привести к ошибке в программе и прекращению ее выполнения если значение опцианала равно nil
 */

// Безопасное извлечение через привязку опционала

milkBottleCount = 17

if let milkBottleCount = milkBottleCount {
    print("Оплата за молоко = ", Double(milkBottleCount) * milkPrice)
} else {
    print("Грузовик с молоком пуст")
}

milkBottleCount = nil
if let milkBottleCount = milkBottleCount {
    print("Оплата за молоко = ", Double(milkBottleCount) * milkPrice)
} else {
    print("Грузовик с молоком пуст")
}

// Дополнительные способы извлечения опционала

// со значением по умолчанию
let unwrappedMilkBottleCount = milkBottleCount ?? 0

//guard применяется внутри функции вроде, тут их пока не прошли



// *** Домашняя работа по теме 2. Функции. Замыкания
//Задача 1

func makeBuffer() -> (String) -> Void {
    var phrase = ""
    
    func increment (arg: String){
        if arg == "" {
            print(phrase)
        }
        phrase += arg
    }
        
    return increment
}

var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")




//Задача 2
func checkPrimeNumber(_ arg: Int) -> Bool {

  // Останавливаем выполнение функции для отрицательных чисел и 1 не является простым
    guard arg > 1 else {
        return false
    }

  // Наименьший делитель составного числа не больше корня из этого числа
    let max = Int(sqrt(Double(arg)))
    
    if max < 2 {
        return true
    }
    
  // Поиск делителей
    for item in 2...max where arg % item == 0 {
    return false
    }

    return true
}

checkPrimeNumber(1)
checkPrimeNumber(2)
checkPrimeNumber(3)
checkPrimeNumber(4)
checkPrimeNumber(8)
checkPrimeNumber(13)


//Домашняя работа №3. На тему Классы, структуры и перечисления
struct Device {
    
    var name: String
    var screenSize: CGSize
    var screenDiagonal: Double
    var scaleFactor: Int
    
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
    
    static let iPhone14Pro = Device(name: "iPhone 14 Pro", screenSize: CGSize(width: 393, height: 852), screenDiagonal: 6.1, scaleFactor: 3)
    static let iPhoneXR = Device(name: "iPhone XR", screenSize: CGSize(width: 414, height: 896), screenDiagonal: 6.06, scaleFactor: 2)
    static let iPadPro = Device(name: "iPad Pro", screenSize: CGSize(width: 843, height: 1194), screenDiagonal: 6.11, scaleFactor: 2)
}

Device.iPhoneXR.screenSize
Device.iPhoneXR.scaleFactor
Device.iPhoneXR.physicalSize()

// Домашняя работа №4. Принципы ООП
// Задание 1. Наследование геометрических фигур

class Shape {
    
    func calculateArea() -> Double {
        var area: Double = 4
        return area
    }
    
    func calculatePerimeter() -> Double {
        var perimeter: Double = 5
        return perimeter
    }
}

class Rectangle: Shape {
    private let height: Double
    private let wight: Double
    
    init (height: Double, wight: Double) {
        self.height = height
        self.wight = wight
    }
    
    override func calculateArea() -> Double{
        return height * wight
    }
    
    override func calculatePerimeter() -> Double {
        return (height * 2) + (wight * 2)
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double{
        return radius * radius * 3.14
    }
    
    override func calculatePerimeter() -> Double {
        return radius * 2 * 3.14
    }
}

class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double{
        return side * side
    }
    
    override func calculatePerimeter() -> Double {
        return side * 4
    }
}


var rectangle = Rectangle(height: 14.56, wight: 15.745879)
rectangle.calculateArea()
rectangle.calculatePerimeter()

var circle = Circle(radius: 12.6)
circle.calculateArea()
circle.calculatePerimeter()

var square = Square(side: 24.154)
square.calculateArea()
square.calculatePerimeter()

//Задача 2. Дженерики

func findIndexWithGenerics<T: Equatable>(ofString valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated(){
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

if let foundIndex = findIndexWithGenerics(ofString: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndex)")
}

let arrayOfInt = [6, 15, 22, 78, 87]
if let foundIndex = findIndexWithGenerics(ofString: 15, in: arrayOfInt) {
    print("Индекс 15: \(foundIndex)")
}

let arrayOfDouble = [6.15, 15.565, 22.67, 78.1, 87.76]
if let foundIndex = findIndexWithGenerics(ofString: 78.1, in: arrayOfDouble) {
    print("Индекс 78.1: \(foundIndex)")
}








